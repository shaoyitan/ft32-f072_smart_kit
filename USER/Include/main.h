/**
  *********************************************************************************
  * @file    	    main.h
  * @author  	    FMD AE
  * @brief   		main Header File. 	
  * @version 	    V1.0.0           
  * @data		    2021-09-27
  *********************************************************************************
  * @attention
  * COPYRIGHT (C) 2021 Fremont Micro Devices Corporation All rights reserved.
  *    This software is provided by the copyright holders and contributors,and the
  *software is believed to be accurate and reliable. However, Fremont Micro Devices
  *Corporation assumes no responsibility for the consequences of use of such
  *software or for any infringement of patents of other rights of third parties,
  *which may result from its use. No license is granted by implication or otherwise
  *under any patent rights of Fremont Micro Devices Corporation.
  *  ******************************************************************************
  */

#ifndef __MAIN_H
#define __MAIN_H


/* Includes ---------------------------------------------------------------------*/
#include "FT32f0xx.h"
#include "oled.h"
/* Public Constant --------------------------------------------------------------*/

void init_adc(void);
/* Public typedef ---------------------------------------------------------------*/
float GetTempture(void);

/* Public define ----------------------------------------------------------------*/
  
#endif /* __MAIN_H */

/************************ (C) COPYRIGHT FMD *****END OF FILE****/
