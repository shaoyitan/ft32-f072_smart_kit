/**
  *********************************************************************************
  * @file    	    oled.h
  * @author  	    FMD AE
  * @brief   		oled Header File. 	
  * @version 	    V1.0.0           
  * @data		    2021-09-27
  *********************************************************************************
  * @attention
  * COPYRIGHT (C) 2021 Fremont Micro Devices Corporation All rights reserved.
  *    This software is provided by the copyright holders and contributors,and the
  *software is believed to be accurate and reliable. However, Fremont Micro Devices
  *Corporation assumes no responsibility for the consequences of use of such
  *software or for any infringement of patents of other rights of third parties,
  *which may result from its use. No license is granted by implication or otherwise
  *under any patent rights of Fremont Micro Devices Corporation.
  *  ******************************************************************************
  */

#ifndef __OLED_H
#define __OLED_H 

/* Includes ---------------------------------------------------------------------*/
#include "FT32f0xx.h"
#include "ft32f0xx_gpio.h"
/* Public Constant --------------------------------------------------------------*/


/* Public typedef ---------------------------------------------------------------*/

/* Public define ----------------------------------------------------------------*/
typedef unsigned          char  u8;
typedef unsigned short    int   u16;
typedef unsigned          int   u32;

#define sOLED_ADDRESS      0x78   /* E2 = 0 */ 
#define sOLED_I2C_TIMING   0xF0210507	//when =SYSCLK=48MHZ
#define IIC_PERIPHERAL   
#define USE_HORIZONTAL 	   90
#define TIMEOUT			   ((uint32_t)100)
#if USE_HORIZONTAL==0||USE_HORIZONTAL==180 //Vertical screen  display
#define oled_x  64
#define oled_y  16
#else                                      //Horizontal screen display
#define oled_x  128
#define oled_y  8
#endif 

#define OLED_SCL_Clr() GPIO_ResetBits(GPIOB,GPIO_Pin_6)//SCL
#define OLED_SCL_Set() GPIO_SetBits(GPIOB,GPIO_Pin_6)

#define OLED_SDA_Clr() GPIO_ResetBits(GPIOB,GPIO_Pin_7)//DIN
#define OLED_SDA_Set() GPIO_SetBits(GPIOB,GPIO_Pin_7)

#define OLED_CMD  0	//Write command
#define OLED_DATA 1	//Write data
/* Public variables -------------------------------------------------------------*/


/* Public function prototypes----------------------------------------------------*/
void OLED_ClearPoint(u8 x,u8 y);
void OLED_ColorTurn(u8 i);
void OLED_DisplayTurn(u8 i);
void I2C_Start(void);
void I2C_Stop(void);
void I2C_WaitAck(void);
void Send_Byte(u8 dat);
void OLED_WR_Byte(u8 dat,u8 mode);
void OLED_DisPlay_On(void);
void OLED_DisPlay_Off(void);
void OLED_Refresh(void);
void OLED_Clear(void);
void OLED_DrawPoint(u8 x,u8 y,u8 t);
void OLED_DrawLine(u8 x1,u8 y1,u8 x2,u8 y2,u8 mode);
void OLED_DrawCircle(u8 x,u8 y,u8 r);
void OLED_ShowChar(u8 x,u8 y,u8 chr,u8 size1,u8 mode);
void OLED_ShowChar6x8(u8 x,u8 y,u8 chr,u8 mode);
void OLED_ShowString(u8 x,u8 y,u8 *chr,u8 size1,u8 mode);
void OLED_ShowNum(u8 x,u8 y,u32 num,u8 len,u8 size1,u8 mode);
void OLED_ShowChinese(u8 x,u8 y,u8 num,u8 size1,u8 mode);
void OLED_ScrollDisplay(u8 num,u8 space,u8 mode);
void OLED_ShowPicture(u8 x,u8 y,u8 sizex,u8 sizey,u8 BMP[],u8 mode);
void OLED_Init(void);

#endif

/************************ (C) COPYRIGHT FMD *****END OF FILE****/
