
/**
  *********************************************************************************
  * @file    	    main.c
  * @author  	    FMD AE
  * @brief   		Main program body  	
  * @version 	    V1.0.0           
  * @data		    2021-09-27
  *********************************************************************************
  * @attention
  * COPYRIGHT (C) 2021 Fremont Micro Devices Corporation All rights reserved.
  *    This software is provided by the copyright holders and contributors,and the
  *software is believed to be accurate and reliable. However, Fremont Micro Devices
  * Corporation assumes no responsibility for the consequences of use of such
  *software or for any infringement of patents of other rights of third parties,
  *which may result from its use. No license is granted by implication or otherwise
  *under any patent rights of Fremont Micro Devices Corporation.
  *  ******************************************************************************
  */

/* Includes ----------------------------------------------------------------------*/
#include "main.h"
#include "bmp.h"
#include "SEGGER_RTT.h"
#include <u8g2.h>
#include <port.h>
#include <croutine.h>
#include <softtimer.h>
#include <jiffies.h>
#include <bsp_led.h>
#include <ft32f0xx_rcc.h>
#include <ft32f0xx_i2c.h>
#define log_d(...) SEGGER_RTT_printf(0, __VA_ARGS__)

//#define USING_SOFT_IIC

//#define log_d(...) ((void)(0))
/* Private Constant --------------------------------------------------------------*/

/* Private typedef ---------------------------------------------------------------*/

/* Private define ----------------------------------------------------------------*/

/* Private variables -------------------------------------------------------------*/

/* Public variables --------------------------------------------------------------*/
tcb_t app_tcb;
#define  STEP 10
u8g2_t  oled_u8g2;    
/* Private function prototypes ---------------------------------------------------*/
void Delay(uint32_t us);
/**********************************************************************************
  * @brief  main program.
  * @param  None
  * @note
  * @retval None
  *********************************************************************************
*/




 uint8_t ft32_msg_cb(u8x8_t *u8x8, uint8_t msg, uint8_t arg_int, void *arg_ptr)
 {
#ifdef  USING_SOFT_IIC
	 switch(msg)
	 {
		 
		 case U8X8_MSG_DELAY_MILLI:
			 board_delay(1);
			 break;
		 case U8X8_MSG_DELAY_10MICRO:
			 board_udelay(10);
			 break;
		 case U8X8_MSG_DELAY_100NANO:
			 __NOP();
			 break;
		 case U8X8_MSG_GPIO_I2C_CLOCK:
			 if(arg_int)
				 OLED_SCL_Set();
			 else
				 OLED_SCL_Clr();
			 break;
		 case U8X8_MSG_GPIO_I2C_DATA:
			 if(arg_int)
				 OLED_SDA_Set();
			 else
				 OLED_SDA_Clr();
			 break;
		 default:
			 return 0;
	 }
	 return 1;
	  #else
	 switch(msg)
	{
		case U8X8_MSG_GPIO_AND_DELAY_INIT:
			break;
		 case U8X8_MSG_DELAY_MILLI:
			 board_delay(arg_int);
			 break;
		case U8X8_MSG_GPIO_I2C_CLOCK:		
			break;							
			
		case U8X8_MSG_GPIO_I2C_DATA:			
			break;
			
		default:	
			return 0;
	}
	return 1; // command processed successfully.
	 #endif
 }

 
 
 void I2C_gpio_init()
 {
	GPIO_InitTypeDef  GPIO_InitStructure;
 	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);	 
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7;	 
 	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT; 		 
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
 	GPIO_Init(GPIOB, &GPIO_InitStructure);	
 	GPIO_SetBits(GPIOB,GPIO_Pin_6 | GPIO_Pin_7);
 }
 void bsp_init()
 {
	 SystemCoreClockUpdate();
	 SysTick_Config(SystemCoreClock/configHZ);
	 SEGGER_RTT_ConfigUpBuffer(0, NULL, NULL, 0, SEGGER_RTT_MODE_NO_BLOCK_SKIP);
	 init_adc();
	 LedConfig(LED1);
	 LedConfig(LED2);
	 LedConfig(LED3);
	 LedConfig(LED4);
	 setup_soft_timer_service();
 }
 
 
 void  show_temp()
 {
	 	char buf[10] = {0};
	 	float temp =GetTempture();
		printf("temp:%f\n",temp);
		
		u8g2_ClearBuffer(&oled_u8g2);	
		
		sprintf(buf,"%0.2f",temp);
        u8g2_SetFont(&oled_u8g2, u8g2_font_unifont_t_chinese1);
		
		u8g2_DrawUTF8(&oled_u8g2, 0,STEP,buf);
		
		u8g2_DrawUTF8(&oled_u8g2, 0,30,"test+opop");
		u8g2_SendBuffer(&oled_u8g2);
		
 }
 
 static void sec_tmr_cb(struct soft_timer *st)
{
	st->expires +=  configHZ;
  soft_timer_add(st);
	st->data ++;
	LedToggle((Led_TypeDef)(st->data&0x03));
	show_temp();
}
 
 static int sec_tmr_init(void)
{
    static struct soft_timer st =
    {
        .cb      = sec_tmr_cb,
				.data	 = STEP,
    };
    st.expires = jiffies + configHZ; 
    soft_timer_add(&st);
    return 0;
}

void IIC_Send(uint8_t *buffer,uint8_t length)
{
	uint32_t TimeOut = TIMEOUT;
	I2C_AcknowledgeConfig(I2C1, ENABLE);
	I2C_TransferHandling(I2C1,sOLED_ADDRESS,length,I2C_SoftEnd_Mode,I2C_Generate_Start_Write);
	/* wait until I2C bus is idle */
	while((--TimeOut) && I2C_GetFlagStatus(I2C1, I2C_ISR_TXE) == RESET);
	if(TimeOut==0) return;//FMPI2C_Err
	
	/* wait until the TXIS bit is set */
	TimeOut = TIMEOUT;
		while((--TimeOut) && I2C_GetFlagStatus(I2C1, I2C_ISR_TXE) == RESET);
	if(TimeOut==0) return;//FMPI2C_Err
	
	
	for(uint8_t i=0;i<length;i++)
	{
	/* data transmission */
			I2C1->TXDR = (uint8_t)buffer[i];
	/* wait until the TC bit is set */
		TimeOut = TIMEOUT;
				while((--TimeOut) && I2C_GetFlagStatus(I2C1, I2C_ISR_TXE) == RESET);
	if(TimeOut==0) return;//FMPI2C_Err

	}
TimeOut = TIMEOUT;
		while((--TimeOut) && I2C_GetFlagStatus(I2C1, I2C_ISR_STOPF) == RESET);
	if(TimeOut==0) return;//FMPI2C_Err

    /* Clear STOPF flag */
  I2C_ClearFlag(I2C1, I2C_ICR_STOPCF);	
}

void oled_IIC_init()
{
		I2C_InitTypeDef I2C_InitStructure;	
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_I2CCLKConfig(RCC_I2C1CLK_SYSCLK);	
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB,ENABLE);	
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1,ENABLE);		
	I2C_DeInit(I2C1);
	
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource6, GPIO_AF_1);
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource7, GPIO_AF_1);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOB, &GPIO_InitStructure);
	
	I2C_InitStructure.I2C_Mode = I2C_Mode_I2C;
	I2C_InitStructure.I2C_AnalogFilter = I2C_AnalogFilter_Disable;
	I2C_InitStructure.I2C_DigitalFilter = 0x00;
	I2C_InitStructure.I2C_OwnAddress1 = 0x00;
	I2C_InitStructure.I2C_Ack = I2C_Ack_Enable;
	I2C_InitStructure.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
	I2C_InitStructure.I2C_Timing = sOLED_I2C_TIMING;
  
	I2C_Init(I2C1, &I2C_InitStructure);
	   
	I2C_Cmd(I2C1, ENABLE);
}
uint8_t u8x8_byte_oled_hw_i2c(u8x8_t *u8x8, uint8_t msg, uint8_t arg_int, void *arg_ptr)
{
	static uint8_t buffer[32];		/* u8g2/u8x8 will never send more than 32 bytes between START_TRANSFER and END_TRANSFER */
	static uint8_t buf_idx;
	uint8_t *data;

	switch(msg)
	{
		case U8X8_MSG_BYTE_SEND:
		  data = (uint8_t *)arg_ptr;      
		  while( arg_int > 0 ){
					buffer[buf_idx++] = *data;
					data++;
					arg_int--;
				}      
		break;
				
		case U8X8_MSG_BYTE_INIT:
		  /* add your custom code to init i2c subsystem */
			oled_IIC_init();
		break;
			
		case U8X8_MSG_BYTE_START_TRANSFER:
		  buf_idx = 0;
		break;
			
		case U8X8_MSG_BYTE_END_TRANSFER:
			IIC_Send(buffer,buf_idx);
		break;
			
		default:
		  return 0;
	}
	return 1;
}



void app_init()
{
	
#ifdef USING_SOFT_IIC  
	I2C_gpio_init();	
	u8g2_Setup_ssd1312_i2c_128x64_noname_f(&oled_u8g2,U8G2_R0,u8x8_byte_sw_i2c,ft32_msg_cb);
#else

	u8g2_Setup_ssd1312_i2c_128x64_noname_f(&oled_u8g2,U8G2_R0,u8x8_byte_oled_hw_i2c,ft32_msg_cb);
#endif
	
	u8g2_InitDisplay(&oled_u8g2);
	u8g2_SetPowerSave(&oled_u8g2,0);
}
static void app_task_cb(struct task_ctrl_blk *tcb, ubase_t data)
{
	 tSTART(tcb);
	 for (;;)
    {
        task_wait_signal(tcb);
        sig_t sig = task_signal(tcb);
        if (sigget(sig, SIG_DATA))
				{
						log_d("get data\n");
				}
				if (sigget(sig, SIG_KEY))
				{
						log_d("get key \n");
				}
    }
    tEND();
}
int main(void)
{
	bsp_init();
	app_init();
	sec_tmr_init();
	task_create(&app_tcb, app_task_cb, 0);
  for (;;)
    {
        task_schedule();
    }	
	
}




/* Private function ------ -------------------------------------------------------*/
/**********************************************************************************
  * @brief  Delay program.
  * @param  None
  * @note
  * @retval None
  *********************************************************************************
*/
void Delay(uint32_t us)
{
	uint32_t i,j;
	for(i=us;i>0;i--)
	{
		for(j=0;j<1000;j++)
		{
			__ASM("NOP");
		}
	}
}
/************************ (C) COPYRIGHT FMD *****END OF FILE****/
//		OLED_Clear();
//		OLED_ShowChinese(18,0,0,16,1);//��
//		OLED_ShowChinese(36,0,1,16,1);//â
//		OLED_ShowChinese(54,0,2,16,1);//΢
//		OLED_ShowChinese(72,0,3,16,1);//��
//		OLED_ShowChinese(90,0,4,16,1);//��
//		OLED_ShowString(18,16,"Fremontmicro",16,1);
//		OLED_ShowString(20,32,"2021/09/17",16,1);
//		OLED_ShowString(0,48,"ASCII:",16,1);  
//		OLED_ShowString(63,48,"CODE:",16,1);
//		OLED_ShowChar(48,48,'!',16,1);//ASCII char
//		OLED_ShowNum(103,48,123,3,16,1);
//		OLED_Refresh();	
//		Delay(1000);
//		OLED_Clear();
//		OLED_ShowPicture(0,0,128,64,BMP3,1);
//		OLED_Refresh();	

