/**
  *********************************************************************************
  * @file    	    oled.c
  * @author  	    FMD AE
  * @brief   		oled program body  	
  * @version 	    V1.0.0           
  * @data		    2021-09-27
  *********************************************************************************
  * @attention
  * COPYRIGHT (C) 2021 Fremont Micro Devices Corporation All rights reserved.
  *    This software is provided by the copyright holders and contributors,and the
  *software is believed to be accurate and reliable. However, Fremont Micro Devices
  * Corporation assumes no responsibility for the consequences of use of such
  *software or for any infringement of patents of other rights of third parties,
  *which may result from its use. No license is granted by implication or otherwise
  *under any patent rights of Fremont Micro Devices Corporation.
  *  ******************************************************************************
  */

/* Includes ----------------------------------------------------------------------*/
#include "oled.h"
#include "stdlib.h"
#include "oledfont.h"
#include <ft32f0xx_i2c.h>
#include <ft32f0xx_rcc.h>

/* Private Constant --------------------------------------------------------------*/

/* Private typedef ---------------------------------------------------------------*/

/* Private define ----------------------------------------------------------------*/

/* Private variables -------------------------------------------------------------*/
u8 OLED_GRAM[144][8];
/* Public variables --------------------------------------------------------------*/

/* Private function prototypes ---------------------------------------------------*/

/**********************************************************************************
  * @brief  OLED_ColorTurn program.Normal or Inverse display.
  * @param  None
  * @note
  * @retval None
  *********************************************************************************
*/
void OLED_ColorTurn(u8 i)
{
	if(i==0)
		{
			OLED_WR_Byte(0xA6,OLED_CMD);//Normal display
		}
	if(i==1)
		{
			OLED_WR_Byte(0xA7,OLED_CMD);// Inverse display
		}
}
/**********************************************************************************
  * @brief  IIC_delay program.
  * @param  None
  * @note
  * @retval None
  *********************************************************************************
*/
void IIC_delay(void)
{
	u8 t=3;
	while(t--);
}

/**********************************************************************************
  * @brief  I2C_Start program.
  * @param  None
  * @note
  * @retval None
  *********************************************************************************
*/
void I2C_Start(void)
{
	OLED_SDA_Set();
	OLED_SCL_Set();
	IIC_delay();
	OLED_SDA_Clr();
	IIC_delay();
	OLED_SCL_Clr();
	IIC_delay();
}

/**********************************************************************************
  * @brief  I2C_Stop program.
  * @param  None
  * @note
  * @retval None
  *********************************************************************************
*/
void I2C_Stop(void)
{
	OLED_SDA_Clr();
	OLED_SCL_Set();
	IIC_delay();
	OLED_SDA_Set();
	
}

/**********************************************************************************
  * @brief  I2C_WaitAck program.
  * @param  None
  * @note
  * @retval None
  *********************************************************************************
*/
void I2C_WaitAck(void) 
{
	OLED_SDA_Set();
	IIC_delay();
	OLED_SCL_Set();
	IIC_delay();
	OLED_SCL_Clr();
	IIC_delay();
}

/**********************************************************************************
  * @brief  Send_Byte program. 
  * @param  None
  * @note
  * @retval None
  *********************************************************************************
*/
void Send_Byte(u8 dat)
{

	u8 i;
	for(i=0;i<8;i++)
	{
		if(dat&0x80)
		{
			OLED_SDA_Set();
    }
		else
		{
			OLED_SDA_Clr();
    }
		IIC_delay();
		OLED_SCL_Set();
		IIC_delay();
		OLED_SCL_Clr();
		dat<<=1;
  }
}

/**********************************************************************************
  * @brief  OLED_WR_Byte program. 
  * @param  mode:data/command  0:command; 1:data;
  * @note
  * @retval None
  *********************************************************************************
*/
void OLED_WR_Byte(u8 dat,u8 mode)
{
#ifdef IIC_PERIPHERAL			
	uint32_t TimeOut = TIMEOUT;
	I2C_AcknowledgeConfig(I2C1, ENABLE);
	I2C_TransferHandling(I2C1,sOLED_ADDRESS,2,I2C_AutoEnd_Mode,I2C_Generate_Start_Write);
	/* wait until I2C bus is idle */
	while((--TimeOut) && I2C_GetFlagStatus(I2C1, I2C_ISR_TXE) == RESET);
	if(TimeOut==0) return;//FMPI2C_Err
	
	/* WriteAddr transmission */
	if(mode){I2C_SendData(I2C1, (uint8_t)0x40);}
	else{I2C_SendData(I2C1, (uint8_t)0x00);}	
	
	/* wait until the TXIS bit is set */
	TimeOut = TIMEOUT;
	while((--TimeOut) && I2C_GetFlagStatus(I2C1, I2C_ISR_TXE) == RESET);
	if(TimeOut==0) return;//FMPI2C_Err
	
	/* data transmission */
	I2C1->TXDR = (uint8_t)dat;
	/* wait until the TC bit is set */
	TimeOut = TIMEOUT;
	while((--TimeOut) && I2C_GetFlagStatus(I2C1, I2C_ISR_TXE) == RESET);
	if(TimeOut==0) return;//FMPI2C_Err
	
	TimeOut = TIMEOUT;
	while((--TimeOut) && I2C_GetFlagStatus(I2C1, I2C_ISR_STOPF) == RESET);
	if(TimeOut==0) return;//FMPI2C_Err
  
    /* Clear STOPF flag */
    I2C_ClearFlag(I2C1, I2C_ICR_STOPCF);	
	
	return;
	
#else	
	I2C_Start();
	Send_Byte(0x78);
	I2C_WaitAck();
	if(mode){Send_Byte(0x40);}
	else{Send_Byte(0x00);}
	I2C_WaitAck();
	Send_Byte(dat);
	I2C_WaitAck();
	I2C_Stop();
#endif
}
/**********************************************************************************
  * @brief  OLED_DisPlay_On program. 
  * @param  None
  * @note	
  * @retval None
  *********************************************************************************
*/
void OLED_DisPlay_On(void)
{
	OLED_WR_Byte(0x8D,OLED_CMD);//Enabling internal charge pump
	OLED_WR_Byte(0x14,OLED_CMD);//Internal charge pump on
	OLED_WR_Byte(0xAF,OLED_CMD);//OLED on
}

/**********************************************************************************
  * @brief  OLED_DisPlay_Off program. 
  * @param  None
  * @note	
  * @retval None
  *********************************************************************************
*/
void OLED_DisPlay_Off(void)
{
	OLED_WR_Byte(0x8D,OLED_CMD);//Disabling internal charge pump
	OLED_WR_Byte(0x10,OLED_CMD);//Internal charge pump off
	OLED_WR_Byte(0xAE,OLED_CMD);//OLED off
}

/**********************************************************************************
  * @brief  OLED_Refresh program. 
  * @param  None
  * @note	
  * @retval None
  *********************************************************************************
*/	
void OLED_Refresh(void)
{
	u8 i,n;
	for(i=0;i<oled_y;i++)
	{
		OLED_WR_Byte(0xb0+i,OLED_CMD); //Set Display Start Line
		OLED_WR_Byte(0x00,OLED_CMD);   // Set Lower Column Start Address for Page 
		OLED_WR_Byte(0x10,OLED_CMD);   // Set HigherColumn Start Address for Page 
		
#ifdef IIC_PERIPHERAL
		uint32_t TimeOut = TIMEOUT;
		I2C_AcknowledgeConfig(I2C1, ENABLE);
		I2C_TransferHandling(I2C1,sOLED_ADDRESS,oled_x+1,I2C_AutoEnd_Mode,I2C_Generate_Start_Write);
		/* wait until I2C bus is idle */
		while((--TimeOut) && I2C_GetFlagStatus(I2C1, I2C_ISR_TXE) == RESET);
		if(TimeOut==0) return;//FMPI2C_Err
		
		/* WriteAddr transmission */
		I2C_SendData(I2C1, (uint8_t)0x40);
		
		/* wait until the TXIS bit is set */
		for(n=0;n<oled_x;n++)
		{
			TimeOut = TIMEOUT;
			while((--TimeOut) && I2C_GetFlagStatus(I2C1, I2C_ISR_TXE) == RESET);
			if(TimeOut==0) return;//FMPI2C_Err
			
			/* data transmission */	
			I2C1->TXDR = (uint8_t)OLED_GRAM[n][i];
		}
		/* wait until the TC bit is set */
		TimeOut = TIMEOUT;
		while((--TimeOut) && I2C_GetFlagStatus(I2C1, I2C_ISR_TXE) == RESET);
		if(TimeOut==0) return;//FMPI2C_Err
		
		TimeOut = TIMEOUT;
		while((--TimeOut) && I2C_GetFlagStatus(I2C1, I2C_ISR_STOPF) == RESET);
		if(TimeOut==0) return;//FMPI2C_Err
	  
		/* Clear STOPF flag */
		I2C_ClearFlag(I2C1, I2C_ICR_STOPCF);		
		
#else		
		I2C_Start();
		Send_Byte(0x78);
		I2C_WaitAck();
		Send_Byte(0x40);
		I2C_WaitAck();
	    for(n=0;n<oled_x;n++)
		{
			Send_Byte(OLED_GRAM[n][i]);
			I2C_WaitAck();
		}
		I2C_Stop();
#endif
  }
}
/**********************************************************************************
  * @brief  OLED_Clear program. 
  * @param  None
  * @note	Clear display
  * @retval None
  *********************************************************************************
*/	
void OLED_Clear(void)
{
	u8 i,n;
	for(i=0;i<8;i++)
	{
	   for(n=0;n<128;n++)
			{
			  OLED_GRAM[n][i]=0;//Clear buff
			}
  }
	OLED_Refresh();//Refresh 
}

/**********************************************************************************
  * @brief  OLED_DrawPoint program. 
  * @param  x:0~127 y:0~63 t:1 fill   0,clear
  * @note	
  * @retval None
  *********************************************************************************
*/	
void OLED_DrawPoint(u8 x,u8 y,u8 t)
{
	u8 i,m,n;
	i=y/8;
	m=y%8;
	n=1<<m;
	if(t){OLED_GRAM[x][i]|=n;}
	else
	{
		OLED_GRAM[x][i]=~OLED_GRAM[x][i];
		OLED_GRAM[x][i]|=n;
		OLED_GRAM[x][i]=~OLED_GRAM[x][i];
	}
}

/**********************************************************************************
  * @brief  OLED_DrawLine program. 
  * @param  x1,y1:The starting coordinates x2,y2:The ending coordinates
  * @note	
  * @retval None
  *********************************************************************************
*/	
void OLED_DrawLine(u8 x1,u8 y1,u8 x2,u8 y2,u8 mode)
{
	u16 t; 
	int xerr=0,yerr=0,delta_x,delta_y,distance;
	int incx,incy,uRow,uCol;
	delta_x=x2-x1; 
	delta_y=y2-y1;
	uRow=x1;
	uCol=y1;
	if(delta_x>0)incx=1; 
	else if (delta_x==0)incx=0;
	else {incx=-1;delta_x=-delta_x;}
	if(delta_y>0)incy=1;
	else if (delta_y==0)incy=0;
	else {incy=-1;delta_y=-delta_x;}
	if(delta_x>delta_y)distance=delta_x; 
	else distance=delta_y;
	for(t=0;t<distance+1;t++)
	{
		OLED_DrawPoint(uRow,uCol,mode);
		xerr+=delta_x;
		yerr+=delta_y;
		if(xerr>distance)
		{
			xerr-=distance;
			uRow+=incx;
		}
		if(yerr>distance)
		{
			yerr-=distance;
			uCol+=incy;
		}
	}
}
/**********************************************************************************
  * @brief  OLED_DrawCircle program. 
  * @param  x,y:The center coordinates r:Radius
  * @note	
  * @retval None
  *********************************************************************************
*/	
void OLED_DrawCircle(u8 x,u8 y,u8 r)
{
	int a, b,num;
    a = 0;
    b = r;
    while(2 * b * b >= r * r)      
    {
        OLED_DrawPoint(x + a, y - b,1);
        OLED_DrawPoint(x - a, y - b,1);
        OLED_DrawPoint(x - a, y + b,1);
        OLED_DrawPoint(x + a, y + b,1);
 
        OLED_DrawPoint(x + b, y + a,1);
        OLED_DrawPoint(x + b, y - a,1);
        OLED_DrawPoint(x - b, y - a,1);
        OLED_DrawPoint(x - b, y + a,1);
        
        a++;
        num = (a * a + b * b) - r*r;
        if(num > 0)
        {
            b--;
            a--;
        }
    }
}

/**********************************************************************************
  * @brief  OLED_ShowChar program. 
  * @param  x:0~127 y:0~63 size:font size  mode:Normal or Inverse display
  * @note	
  * @retval None
  *********************************************************************************
*/
void OLED_ShowChar(u8 x,u8 y,u8 chr,u8 size1,u8 mode)
{
	u8 i,m,temp,size2,chr1;
	u8 x0=x,y0=y;
	if(size1==8)size2=6;
	else size2=(size1/8+((size1%8)?1:0))*(size1/2);  //Gets the number of bytes used by a character
	chr1=chr-' ';  //Get offset
	for(i=0;i<size2;i++)
	{
		if(size1==8)
			  {temp=asc2_0806[chr1][i];} //Call 0806 font
		else if(size1==12)
        {temp=asc2_1206[chr1][i];} //Call 1206 font
		else if(size1==16)
        {temp=asc2_1608[chr1][i];} //Call 1608 font
		else if(size1==24)
        {temp=asc2_2412[chr1][i];} //Call 2412 font
		else return;
		for(m=0;m<8;m++)
		{
			if(temp&0x01)OLED_DrawPoint(x,y,mode);
			else OLED_DrawPoint(x,y,!mode);
			temp>>=1;
			y++;
		}
		x++;
		if((size1!=8)&&((x-x0)==size1/2))
		{x=x0;y0=y0+8;}
		y=y0;
  }
}


/**********************************************************************************
  * @brief  OLED_ShowString program. 
  * @param  x:0~127 y:0~63 
  *         size:font size  
  *         *chr:Start address of string  
  *         mode:Normal or Inverse display
  * @note	
  * @retval None
  *********************************************************************************
*/
void OLED_ShowString(u8 x,u8 y,u8 *chr,u8 size1,u8 mode)
{
	while((*chr>=' ')&&(*chr<='~'))//Determine whether it is illegal
	{
		OLED_ShowChar(x,y,*chr,size1,mode);
		if(size1==8)x+=6;
		else x+=size1/2;
		chr++;
	}
}

/**********************************************************************************
  * @brief  OLED_Pow program. 
  * @param  m:0~127 n:0~63 
  * @note	
  * @retval None
  *********************************************************************************
*/
u32 OLED_Pow(u8 m,u8 n)
{
	u32 result=1;
	while(n--)
	{
	  result*=m;
	}
	return result;
}

/**********************************************************************************
  * @brief  OLED_ShowNum program. 
  * @param  m:0~127 n:0~63 
  *		      num:Display number 
  *         len:number lentgh
  *         size1:font size
  *         mode:Normal or Inverse display
  * @note	
  * @retval None
  *********************************************************************************
*/
void OLED_ShowNum(u8 x,u8 y,u32 num,u8 len,u8 size1,u8 mode)
{
	u8 t,temp,m=0;
	if(size1==8)m=2;
	for(t=0;t<len;t++)
	{
		temp=(num/OLED_Pow(10,len-t-1))%10;
		if(temp==0)
		{
			OLED_ShowChar(x+(size1/2+m)*t,y,'0',size1,mode);
		}
		else 
		{
			OLED_ShowChar(x+(size1/2+m)*t,y,temp+'0',size1,mode);
		}
	}
}

/**********************************************************************************
  * @brief  OLED_ShowChinese program. 
  * @param  m:0~127 n:0~63 
  *		    num:The serial number of Chinese characters
  *         size1:font size
  *         mode:Normal or Inverse display
  * @note	
  * @retval None
  *********************************************************************************
*/
void OLED_ShowChinese(u8 x,u8 y,u8 num,u8 size1,u8 mode)
{
	u8 m,temp;
	u8 x0=x,y0=y;
	u16 i,size3=(size1/8+((size1%8)?1:0))*size1;  //Gets the number of bytes used by a character
	for(i=0;i<size3;i++)
	{
		if(size1==16)
				{temp=Hzk1[num][i];}////Call 16*16 font
		else if(size1==24)
				{temp=Hzk2[num][i];}//Call 24*24 font
		else if(size1==32)       
				{temp=Hzk3[num][i];}//Call 32*32 font
		else if(size1==64)
				{temp=Hzk4[num][i];}//Call 64*64 font
		else return;
		for(m=0;m<8;m++)
		{
			if(temp&0x01)OLED_DrawPoint(x,y,mode);
			else OLED_DrawPoint(x,y,!mode);
			temp>>=1;
			y++;
		}
		x++;
		if((x-x0)==size1)
		{x=x0;y0=y0+8;}
		y=y0;
	}
}

/**********************************************************************************
  * @brief  OLED_ScrollDisplay program. 
  * @param  num:The number of Chinese
  *		    space:According to interval
  *         mode:Normal or Inverse display
  * @note	
  * @retval None
  *********************************************************************************
*/
void OLED_ScrollDisplay(u8 num,u8 space,u8 mode)
{
	u8 i,n,t=0,m=0,r;
	while(1)
	{
		if(m==0)
		{
	    OLED_ShowChinese(128,24,t,16,mode); 
			t++;
		}
		if(t==num)
			{
				for(r=0;r<16*space;r++)      
				 {
					for(i=1;i<144;i++)
						{
							for(n=0;n<8;n++)
							{
								OLED_GRAM[i-1][n]=OLED_GRAM[i][n];
							}
						}
           OLED_Refresh();
				 }
        t=0;
      }
		m++;
		if(m==16){m=0;}
		for(i=1;i<144;i++)  
		{
			for(n=0;n<8;n++)
			{
				OLED_GRAM[i-1][n]=OLED_GRAM[i][n];
			}
		}
		OLED_Refresh();
	}
}

/**********************************************************************************
  * @brief  OLED_ShowChinese program. 
  * @param  m:0~127 n:0~63 
  *		    sizex:Length of the picture
  *         sizey:Width of the picture
  *         BMP[]:BUFFER of the picture
  *         mode:Normal or Inverse display
  * @note	
  * @retval None
  *********************************************************************************
*/
void OLED_ShowPicture(u8 x,u8 y,u8 sizex,u8 sizey,u8 BMP[],u8 mode)
{
	u16 j=0;
	u8 i,n,temp,m;
	u8 x0=x,y0=y;
	sizey=sizey/8+((sizey%8)?1:0);
	for(n=0;n<sizey;n++)
	{
		 for(i=0;i<sizex;i++)
		 {
				temp=BMP[j];
				j++;
				for(m=0;m<8;m++)
				{
					if(temp&0x01)OLED_DrawPoint(x,y,mode);
					else OLED_DrawPoint(x,y,!mode);
					temp>>=1;
					y++;
				}
				x++;
				if((x-x0)==sizex)
				{
					x=x0;
					y0=y0+8;
				}
				y=y0;
		}
	}
}
/**********************************************************************************
  * @brief  OLED_Init program. 
  * @param  None
  * @note	
  * @retval None
  *********************************************************************************
*/
void OLED_Init(void)
{
#ifdef IIC_PERIPHERAL
	I2C_InitTypeDef I2C_InitStructure;	
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_I2CCLKConfig(RCC_I2C1CLK_SYSCLK);	
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB,ENABLE);	
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1,ENABLE);		
	I2C_DeInit(I2C1);
	
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource6, GPIO_AF_1);
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource7, GPIO_AF_1);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOB, &GPIO_InitStructure);
	
	I2C_InitStructure.I2C_Mode = I2C_Mode_I2C;
	I2C_InitStructure.I2C_AnalogFilter = I2C_AnalogFilter_Disable;
	I2C_InitStructure.I2C_DigitalFilter = 0x00;
	I2C_InitStructure.I2C_OwnAddress1 = 0x00;
	I2C_InitStructure.I2C_Ack = I2C_Ack_Enable;
	I2C_InitStructure.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
	I2C_InitStructure.I2C_Timing = sOLED_I2C_TIMING;
  
	I2C_Init(I2C1, &I2C_InitStructure);
	   
	I2C_Cmd(I2C1, ENABLE);	
	
#else
	GPIO_InitTypeDef  GPIO_InitStructure;
 	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);	 
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7;	 
 	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT; 		 
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
 	GPIO_Init(GPIOB, &GPIO_InitStructure);	
 	GPIO_SetBits(GPIOB,GPIO_Pin_6 | GPIO_Pin_7);
#endif
	OLED_WR_Byte(0xAE,OLED_CMD);	//--turn off oled panel
	
	OLED_WR_Byte(0x00,OLED_CMD);    /*set lower column address*/       
	OLED_WR_Byte(0x10,OLED_CMD);    /*set higher column address*/

	OLED_WR_Byte(0xB0,OLED_CMD);    /*set page address*/

	OLED_WR_Byte(0x81,OLED_CMD);    /*contract control*/
	OLED_WR_Byte(0x5f,OLED_CMD);    /*128*/
	
	if(USE_HORIZONTAL==0)
	{
		OLED_WR_Byte(0x20,OLED_CMD);    /* Set Memory addressing mode (0x20/0x21) */
		OLED_WR_Byte(0x09,OLED_CMD);     /* 0x09 */

		OLED_WR_Byte(0xA1,OLED_CMD);    /*set segment remap  0XA1 */
		OLED_WR_Byte(0xC8,OLED_CMD);    /*Com scan direction   0Xc8  */
	}
	else if(USE_HORIZONTAL==90)
	{
		OLED_WR_Byte(0x20,OLED_CMD);    /* Set Memory addressing mode (0x20/0x21) */
		OLED_WR_Byte(0x02,OLED_CMD);     /* 0x02 */

		OLED_WR_Byte(0xA1,OLED_CMD);    /*set segment remap  0XA1 */
		OLED_WR_Byte(0xC0,OLED_CMD);    /*Com scan direction   0Xc0  */
	}
	else if(USE_HORIZONTAL==180)
	{
		OLED_WR_Byte(0x20,OLED_CMD);    /* Set Memory addressing mode (0x20/0x21) */
		OLED_WR_Byte(0x09,OLED_CMD);     /* 0x09 */

		OLED_WR_Byte(0xA0,OLED_CMD);    /*set segment remap  0XA0 */
		OLED_WR_Byte(0xC0,OLED_CMD);    /*Com scan direction   0Xc0  */
	}
	else if(USE_HORIZONTAL==270)
	{
		OLED_WR_Byte(0x20,OLED_CMD);    /* Set Memory addressing mode (0x20/0x21) */
		OLED_WR_Byte(0x02,OLED_CMD);     /* 0x02 */

		OLED_WR_Byte(0xA0,OLED_CMD);    /*set segment remap  0XA0 */
		OLED_WR_Byte(0xC8,OLED_CMD);    /*Com scan direction   0Xc8  */
	}
	
	OLED_WR_Byte(0xA4,OLED_CMD);    /*Disable Entire Display On (0xA4/0xA5)*/ 

	OLED_WR_Byte(0xA6,OLED_CMD);    /*normal / reverse*/

	OLED_WR_Byte(0xA8,OLED_CMD);    /*multiplex ratio*/
	OLED_WR_Byte(0x3F,OLED_CMD);    /*duty = 1/64*/

	OLED_WR_Byte(0xD3,OLED_CMD);    /*set display offset*/
	OLED_WR_Byte(0x00,OLED_CMD);    /*   0x20   */

	OLED_WR_Byte(0xD5,OLED_CMD);    /*set osc division*/
	OLED_WR_Byte(0x80,OLED_CMD);    

	OLED_WR_Byte(0xD9,OLED_CMD);    /*set pre-charge period*/
	OLED_WR_Byte(0x22,OLED_CMD);

	OLED_WR_Byte(0xDA,OLED_CMD);    /* Set SEG Pins Hardware Configuration */
	OLED_WR_Byte(0x10,OLED_CMD);

	OLED_WR_Byte(0xdb,OLED_CMD);    /*set vcomh*/
	OLED_WR_Byte(0x30,OLED_CMD);

	OLED_WR_Byte(0x8d,OLED_CMD);    /*set charge pump enable*/
	OLED_WR_Byte(0x72,OLED_CMD);    /* 0x12:7.5V; 0x52:8V;  0x72:9V;  0x92:10V */
	OLED_Clear();
	OLED_WR_Byte(0xAF,OLED_CMD);
}

