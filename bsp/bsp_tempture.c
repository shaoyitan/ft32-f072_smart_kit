#include "main.h"
#include "SEGGER_RTT.h"
#include <u8g2.h>
#include <port.h>
#include <croutine.h>
#include <softtimer.h>
#include <jiffies.h>
#include <bsp_led.h>

#include "ft32f0xx.h"

#define TS_CAL1_ADR  0x1FFFF7B8
#define TS_CAL2_ADR  0x1FFFF7C2

#define TS_CAL1_TEMPTURE  25 //25�c
#define TS_CAL2_TEMPTURE  110//110�c

void init_adc()
{
	ADC_InitTypeDef     ADC_InitStructure;
//  GPIO_InitTypeDef    GPIO_InitStructure;
//  
//  /* GPIOC Periph clock enable */
//	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
  
  /* ADC1 Periph clock enable */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
  
//  /* Configure ADC Channel11 as analog input */
//	 GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3 ;
//  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
//  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL ;
//	 GPIO_Init(GPIOA, &GPIO_InitStructure);
//  
//  /* ADCs DeInit */  
//  ADC_DeInit(ADC1);
  
  /* Initialize ADC structure */
  ADC_StructInit(&ADC_InitStructure);
  
  /* Configure the ADC1 in continuous mode with a resolution equal to 12 bits  */
  ADC_InitStructure.ADC_Resolution = ADC_Resolution_12b;
  ADC_InitStructure.ADC_ContinuousConvMode = ENABLE; 
  ADC_InitStructure.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_None;
  ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
  ADC_InitStructure.ADC_ScanDirection = ADC_ScanDirection_Upward;
  ADC_Init(ADC1, &ADC_InitStructure); 


	ADC_ChannelConfig(ADC1, ADC_Channel_TempSensor , ADC_SampleTime_239_5Cycles);
  /* ADC Calibration */
  ADC_GetCalibrationFactor(ADC1);
  
  /* Enable the ADC peripheral */
  ADC_Cmd(ADC1, ENABLE);     
  
  /* Wait the ADRDY flag */
  while(!ADC_GetFlagStatus(ADC1, ADC_FLAG_ADRDY)); 
  
	ADC_TempSensorCmd(ENABLE);
	ADC_VrefintCmd(ENABLE);
	
	/* ADC1 regular Software Start Conv */ 
  ADC_StartOfConversion(ADC1);
}
float GetTempture(void)
{
	uint16_t TS_CAL1,TS_CAL2,CurrAdcValue;
	float tempValue,CurrTempture;
	
	TS_CAL1 = *(uint16_t *)(TS_CAL1_ADR);	//???????1
	TS_CAL1 &= 0x0fff;
	TS_CAL2 = *(uint16_t *)(TS_CAL2_ADR);//???????2
	TS_CAL2 &= 0x0fff;
	
	/* Test EOC flag */
	while(ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET);
			
			/* Get ADC1 converted data */
	CurrAdcValue =ADC_GetConversionValue(ADC1);
	
	tempValue = (TS_CAL2_TEMPTURE - TS_CAL1_TEMPTURE)*(CurrAdcValue - TS_CAL1);
	tempValue /=(TS_CAL2 - TS_CAL1);
	tempValue += TS_CAL1_TEMPTURE;
	CurrTempture = tempValue;
	
	return CurrTempture;
}
