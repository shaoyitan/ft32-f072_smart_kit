/**
	******************************************************************************
	* @file 		bsp_led.h
	* @author 	FMD AE
	* @brief		Header for main.c module
	* @version 	V1.0.0
	* @data 		2021-09-14
	******************************************************************************
	* @attention
	* COPYRIGHT (C) 2021 Fremont Micro Devices (SZ) Corporation All rights reserved.
	* This software is provided by the copyright holders and contributors,and the
	*	software is believed to be accurate and reliable. However, Fremont Micro
	*	Devices (SZ) Corporation assumes no responsibility for the consequences of
	*	use of such software or for any infringement of patents of other rights
	*	of third parties, which may result from its use. No license is granted by
	*	implication or otherwise under any patent rights of Fremont Micro Devices (SZ)
	*	Corporation.
	******************************************************************************
	*/
	
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef	__BSP_LED_H
#define	__BSP_LED_H

/* Includes ------------------------------------------------------------------*/
#include "ft32f0xx.h"
#include "ft32f0xx_gpio.h"

/* Exported types ------------------------------------------------------------*/
typedef	struct
{
	uint32_t clk;
	GPIO_TypeDef*	port;
	uint16_t	pin;
	BitAction	active_state;
	uint8_t reserved;
}PIN_CONFIG_t;

typedef enum 
{
  LED1 = 0,
  LED2 = 1,
  LED3 = 2,
  LED4 = 3
} Led_TypeDef;

/* Exported constants --------------------------------------------------------*/
/* ------------------------------- Configuration ---------------------------- */
#define LED_NUM                         4U

/* Exported macro ------------------------------------------------------------*/
#define	LED1_CLK		RCC_AHBENR_GPIOBEN
#define	LED2_CLK		RCC_AHBENR_GPIOBEN
#define	LED3_CLK		RCC_AHBENR_GPIOBEN
#define	LED4_CLK		RCC_AHBENR_GPIOBEN

#define	LED1_GPIOX	GPIOB
#define	LED2_GPIOX	GPIOB
#define	LED3_GPIOX	GPIOB
#define	LED4_GPIOX	GPIOB

#define	LED1_GPIO_Pin	GPIO_Pin_0
#define	LED2_GPIO_Pin	GPIO_Pin_1
#define	LED3_GPIO_Pin	GPIO_Pin_2
#define	LED4_GPIO_Pin	GPIO_Pin_3

/* Exported functions ------------------------------------------------------- */
void LedConfig(Led_TypeDef Led);
void LedOn(Led_TypeDef Led);												
void LedOff(Led_TypeDef Led);												
void LedToggle(Led_TypeDef Led);

#endif	/* __BSP_LED_H */
												
/************************ (C) COPYRIGHT Fremont Micro Devices *****END OF FILE****/												




