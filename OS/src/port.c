#include <types.h>
#include "SEGGER_RTT.h"
#include "port.h"
#include <jiffies.h>

#define port_SYSTICK_CLK_BIT    (1UL << 2UL)
#define port_SYSTICK_INT_BIT    (1UL << 1UL)
#define port_SYSTICK_ENABLE_BIT (1UL << 0UL)

int setup_tick_service(void)
{
	SysTick->LOAD = (SystemCoreClock / configHZ) - 1UL;
	SysTick->VAL = 0;
  SysTick->CTRL = (port_SYSTICK_CLK_BIT | port_SYSTICK_INT_BIT | port_SYSTICK_ENABLE_BIT);

   return 0;
}


void board_delay(uint32_t ms)
{
	uint32_t tick  = 0;
	tick = jiffies;
	while(jiffies - tick < ms);
}
void board_udelay(unsigned long us)
{


	uint32_t i,j;
	/*when sysclk equal 48M ,the delay is about 1us*/
	for(i=0;i<us;i++)	
	{ 
		for(j=0;j<6;j++)
		{
			__NOP();
		}
	}
}

