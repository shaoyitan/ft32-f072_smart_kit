#ifndef _PORT_H_
#define _PORT_H_

#include <types.h>
#include <ft32f0xx.h>
#include <core_cm0.h>

#define configHZ 1000

#define enable_irq()      __enable_irq()
#define disable_irq()     __disable_irq()

#define portBYTE_ALIGNMENT      (4)
#define portBYTE_ALIGNMENT_MASK (0x03)

#define OS_CPU_SR   uint32_t
#define enter_critical()        \
    do { cpu_sr = __get_PRIMASK(); disable_irq();} while (0)
#define exit_critical()         \
    do { __set_PRIMASK(cpu_sr);} while (0)

#define portDISABLE_INTERRUPTS __disable_irq

int setup_tick_service(void);
void board_delay(uint32_t ms);
void board_udelay(unsigned long us);
#endif

