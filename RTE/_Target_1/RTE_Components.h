
/*
 * Auto generated Run-Time-Environment Component Configuration File
 *      *** Do not modify ! ***
 *
 * Project: 'BoardDemo' 
 * Target:  'Target 1' 
 */

#ifndef RTE_COMPONENTS_H
#define RTE_COMPONENTS_H


/*
 * Define the Device Header File: 
 */
#define CMSIS_device_header "ft32f0xx.h"

#define RTE_DEVICE_ADC
#define RTE_DEVICE_COMMON
#define RTE_DEVICE_DMA
#define RTE_DEVICE_FLASH
#define RTE_DEVICE_GPIO
#define RTE_DEVICE_I2C
#define RTE_DEVICE_RCC
#define RTE_DEVICE_STARTUP_FT32F0XX    /* Device Startup for FT32F0 */

#endif /* RTE_COMPONENTS_H */
